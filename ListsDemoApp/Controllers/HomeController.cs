﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ListsDemoApp.Models;

namespace ListsDemoApp.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var fooModels = new List<FooModel>
            {
                new FooModel(1,"17 Tsanzaguru Rusape","wah@umm.com"),
                new FooModel(1,"138 Heatherdale","wah@umm.com"),
                new FooModel(1,"17 Tsanzaguru Rusape","wah@umm.com")
            };

            // Method 1
            ViewBag.dddString = new JavaScriptSerializer().Serialize(fooModels);

            // Method 2
            ViewBag.dddList = fooModels;
            

            return View(fooModels);
        }
    }
}
