﻿namespace ListsDemoApp.Models
{
    public class FooModel
    {
        public FooModel(int docid, string address, string email)
        {
            this.docid = docid;
            this.address = address;
            this.email = email;
        }

        public FooModel()
        {
        }

        public int docid { get; set; }
        public string address  { get; set; }
        public string email { get; set; }
    }
}